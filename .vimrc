"
"    ██╗   ██╗██╗███╗   ███╗██████╗  ██████╗
"    ██║   ██║██║████╗ ████║██╔══██╗██╔════╝
"    ██║   ██║██║██╔████╔██║██████╔╝██║
"    ╚██╗ ██╔╝██║██║╚██╔╝██║██╔══██╗██║
"  ██╗╚████╔╝ ██║██║ ╚═╝ ██║██║  ██║╚██████╗
"  ╚═╝ ╚═══╝  ╚═╝╚═╝     ╚═╝╚═╝  ╚═╝ ╚═════╝
"
" By: T. M. Ghazi Al - Hizmi, Aceh, Indonesia
let mapleader =","

" vim-plug automatic installation in vim (git command needed)
" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin('~/.vim/plugged')
    Plug 'morhetz/gruvbox'
    Plug 'vim-airline/vim-airline'
    Plug 'edkolev/tmuxline.vim'
    Plug 'ap/vim-css-color'
    Plug 'ryanoasis/vim-devicons'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-surround'
    Plug 'airblade/vim-gitgutter'
    Plug 'mhinz/vim-startify'
    Plug 'vimwiki/vimwiki'
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    " Plug 'Yggdroot/indentLine'
call plug#end()

" Some basics:
" Must be first before other
set nocompatible
filetype plugin on
syntax on
set encoding=utf-8
set number relativenumber
set noshowmode     " remove default mode indicator
set textwidth=80   " wrap line automatically if it width reach this number
set colorcolumn=80 " highlight maximum text width
" set cursorcolumn   " show column cursor
" set cursorline     " show line cursor

set go=a
set mouse=a
set incsearch        " find search character(s) as typing
set hlsearch         " highlight search result
" set nohlsearch     " don't highlight search result
set clipboard=unnamedplus
" Search ignore case, on and off
set ignorecase

" Gruvbox colorscheme
" Must set before colorscheme
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
let g:gruvbox_italic=1
set background=dark     " Setting dark mode
"set background=light   " Setting light mode
"set termguicolors

" Populate let:g:airline_symbols dictionary with the powerline symbols
let g:airline_powerline_fonts = 1

" Disabled airline-tmuxline overwrite my theme
let g:airline#extensions#tmuxline#enabled = 0

" Choose tmuxline stock preset
let g:tmuxline_preset = 'powerline'

let g:vimwiki_list = [{'path': '/home/tmgalhizmi/nc/opmitdti/vimwiki'}]

" Source Vim configuration file and install plugins
nnoremap <silent><leader>1 :source ~/.vimrc \| :PlugInstall<CR>

" Costumize netrw
let g:netrw_banner=0
let g:netrw_liststyle=3
let g:netrw_winsize=20

" Remap keys
no <Up> <Nop>
no <Down> <Nop>
no <Left> <Nop>
no <Right> <Nop>

ino <Up> <Nop>
ino <Down> <Nop>
ino <Left> <Nop>
ino <Right> <Nop>

"" From https://www.youtube.com/watch?v=I0PrxH53Rfc
" Paste last yank, not from deleted
nmap ,p "0p
nmap ,P "0P

" Navigating with guides
autocmd Filetype tex,bib,markdown inoremap <Tab><Space> <Esc>/<++><Enter>c4l
autocmd Filetype tex,bib,markdown vnoremap <Tab><Space> <Esc>/<++><Enter>c4l
autocmd Filetype tex,bib,markdown map <Tab><Space> <Esc>/<++><Enter>c4l
autocmd Filetype tex,bib,markdown inoremap ;gui <++>

" Latex code snippets
autocmd FileType tex inoremap ;fr \begin{frame}<Enter>\frametitle{}<Enter><Enter><++><Enter><Enter>\end{frame}<Enter><Enter><++><Esc>6kf}i
autocmd Filetype tex inoremap ;em \emph{}<++><Esc>T{i
autocmd Filetype tex inoremap ;bf \textbf{}<++><Esc>T{i
autocmd Filetype tex inoremap ;it \textit{}<++><Esc>T{i
autocmd Filetype tex inoremap ;ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd Filetype tex inoremap ;ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><++><Esc>3kA\item<Space>
autocmd Filetype tex inoremap ;li <Enter>\item<Space>
autocmd Filetype tex inoremap ;tab \begin{tabular}<Enter><++><Enter>\end{tabular}<Enter><Enter><++><Esc>4kA{}<Esc>i
autocmd Filetype tex inoremap ;a \href{}{<++>}<Space><++><Esc>2T{i
autocmd Filetype tex inoremap ;chap \chapter{}<Enter><Enter><++><Esc>2kf}i
autocmd Filetype tex inoremap ;sec \section{}<Enter><Enter><++><Esc>2kf}i
autocmd Filetype tex inoremap ;ssec \subsection{}<Enter><Enter><++><Esc>2kf}i
autocmd Filetype tex inoremap ;sssec \subsubsection{}<Enter><Enter><++><Esc>2kf}i
autocmd Filetype tex inoremap ;up <Esc>/usepackage<Enter>o\usepackage{}<Esc>i
autocmd Filetype tex nnoremap ;up /usepackage<Enter>o\usepackage{}<Esc>i
"autocmd Filetype tex inoremap ;col \begin{columns}[T]<Enter>\begin{column}{.5\textwidth}<Enter><Enter>\end{column}<Enter>\begin{column}{.5\textwidth}<Enter><++><Enter>\end{column}<Enter>\end{columns}<Esc>5kA

" .bib
autocmd FileType bib inoremap ,a @article{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>journal<Space>=<Space>{<++>},<Enter>volume<Space>=<Space>{<++>},<Enter>pages<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i
autocmd FileType bib inoremap ,b @book{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>6kA,<Esc>i
autocmd FileType bib inoremap ,c @incollection{<Enter>author<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>booktitle<Space>=<Space>{<++>},<Enter>editor<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i

" Markdown
autocmd Filetype markdown inoremap ;n ---<Enter><Enter>
autocmd Filetype markdown inoremap ;b ****<++><Esc>F*hi
autocmd Filetype markdown inoremap ;s ~~~~<++><Esc>F~hi
autocmd Filetype markdown inoremap ;h ====<Space><++><Esc>F=hi
autocmd Filetype markdown inoremap ;i ![](<++>)<++><Esc>F[a
autocmd Filetype markdown inoremap ;a [](<++>)<++><Esc>F[a
autocmd Filetype markdown inoremap ;1 #<Space><Enter><++><Esc>kA
autocmd Filetype markdown inoremap ;2 ##<Space><Enter><++><Esc>kA
autocmd Filetype markdown inoremap ;3 ###<Space><Enter><++><Esc>kA
autocmd Filetype markdown inoremap ;l --------<Enter>
"autocmd Filetype markdown map <F5> :!pandoc<space><C-r>%<space>-o<space><C-r>%.pdf<Enter><Enter>

" Compile and open tex file with zathura
nnoremap <leader>w :w <bar> ! pdflatex -synctex=1 -interaction=nonstopmode %:p<CR>
nnoremap <leader>z :! zathura %:p:r.pdf &<CR>

" Start asyncrun for latexmk
"nnoremap <leader>cp :AsyncRun latexmk -pvc -pdf %<CR>

" Clean temporary files
"nnoremap <leader>cf :!latexmk -c <CR>

" Text, tab and indent related
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

" Enable autocompletion:
set wildmode=longest,list,full

" Spell check, 'o' means 'orthography'
map <leader>o :setlocal spell! spelllang=id<CR>

" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
set splitbelow splitright

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Function {{{
" trailing whitespace
match ErrorMsg '\s\+$'
function! TrimWhiteSpace()
	%s/\s\+$//e
endfunction
autocmd BufWritePre * :call TrimWhiteSpace()
" }}}

" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufWritePost *Xresources,*Xdefaults,*xresources !xrdb %

""" nnn
" Floating window (neovim latest and vim with patch 8.2.191)
let g:nnn#layout = { 'window': { 'width': 0.4, 'height': 0.6, 'xoffset': 1, 'highlight': 'Comment', 'border': 'rounded' }}

let g:nnn#action = {
      \ '<c-t>': 'tab split',
      \ '<c-x>': 'split',
      \ '<c-v>': 'vsplit' }

""" IndentLine
" Change conceal behaviour
let g:indentLine_concealcursor = ''		" show when cursor in line
"let g:indentLine_setConceal = 0 		" disable conceal
let g:indentLine_fileTypeExclude = [ 'startify' ]

""" Startify
let g:startify_lists = [
      \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
      \ { 'type': 'files',     'header': ['   MRU']            },
      \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
      \ { 'type': 'sessions',  'header': ['   Sessions']       },
      \ { 'type': 'commands',  'header': ['   Commands']       },
      \ ]

let g:startify_bookmarks = [
    \ { 'c': '~/.vimrc' },
    \ { 'a': '~/.config/awesome/rc.lua' },
    \ { 'r': '~/.config/ranger/rc.conf' },
    \ { 't': '~/nc/opmitdti/operator/laporan-fingerprint/rincian-telat.md' },
\]
