#
# ~/.bash_profile
#
# Uncomment this if login using display manager
#[[ -f ~/.bashrc ]] && source ~/.bashrc

# Default programs
export PATH="$PATH:$HOME/.local/bin"
export EDITOR="nvim"
export VISUAL="nvim"
export READER="zathura"
export BROWSER="qutebrowser"
export QT_QPA_PLATFORMTHEME="qt5ct"

# export QT_STYLE_OVERRIDE="kvantum"
#export TERMINAL="alacritty"
#export FILE="pcmanfm"
export RANGER_LOAD_DEFAULT_RC=FALSE

# Use Adwaita-dark theme for Qt5 apps
# export QT_STYLE_OVERRIDE=Adwaita-dark

# ~/ Clean-up:
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority" # This line will break some DMs.
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"
#export WGETRC="$HOME/.config/wget/wgetrc"
export ZDOTDIR="$HOME/.config/zsh/zshrc"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DOCUMENTS_DIR=$HOME/doc
export XDG_DOWNLOAD_DIR=$HOME/don
export XDG_MUSIC_DIR=$HOME/mux
export XDG_TEMPLATES_DIR=$HOME/temps
export XDG_PICTURES_DIR=$HOME/pix
export XDG_VIDEOS_DIR=$HOME/vid
#export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
# export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"

# NNN config
export NNN_BMS="c:~/nc/pribadi/doc/projek-pribadi/python;d:~/nc/opmitdti/data_siswa/2021-2022;m:/run/media/$USER;l:~/doc/Tutorials/gnu-linux_tutorials;v:~/vid/GNU+Linux-Videos"
export NNN_USE_EDITOR=1
export NNN_COLORS="6354"
export NNN_PLUG='p:preview-tui;f:fzopen;c:fzcd;l:launch;g:-_|gimp $nnn;o:-_|libreoffice $nnn;w:_setwal $nnn*'
export NNN_OPENER="$HOME/.config/nnn/plugins/nuke"
export NNN_FIFO=/tmp/nnn.fifo
export NNN_TRASH=1
export NNN_ARCHIVE="\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$"

# Autostart X server without display manager
# To exit from DE or WM, type this command `pkill <de_or_wm_name>`
if shopt -q login_shell; then
    [[ -f ~/.bashrc ]] && source ~/.bashrc
    [[ -t 0 && $(tty) == /dev/tty1 && ! $DISPLAY ]] && exec startx
else
    exit 1 # Somehow this is non-bash or non-login shell
fi
