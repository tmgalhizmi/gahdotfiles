# Lines configured by zsh-newuser-install
autoload -U colors && colors

# Luke prompt
#PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

# Maia prompt
PS1="%B%{$fg[cyan]%}%(4~|%-1~/.../%2~|%~)%u%b >%{$fg[cyan]%}>%B%(?.%{$fg[cyan]%}.%{$fg[red]%})>%{$reset_color%}%b " # Print some system information when the shell is first started

# Load zsh plugins
if [ -d $HOME/.config/zsh-plugins/ ]; then
    source $HOME/.config/zsh/plugins/colored-man-pages.plugin.zsh
    source $HOME/.config/zsh/plugins/compleat.plugin.zsh
fi

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit

# Include hidden files in autocomplete:
_comp_options+=(globdots)

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

export KEYTIMEOUT=1

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# ZSH fitures enabled
setopt autocd # auto cd
setopt extendedglob # extending globbing queries ex: 'cp ^*.(tar

# Keybind for v
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
#zstyle :compinstall filename '/home/tmgalhizmi/.zshrc'
#
#autoload -Uz compinit
#compinit
# End of lines added by compinstall

# Separate path for .zcompdump file
compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION

[ -f ~/.config/aliasrc ] && . ~/.config/aliasrc

# Fish-like syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# The "command not found" handler
source /usr/share/doc/pkgfile/command-not-found.zsh

# NNN prompt indicator
[ -n "$NNNLVL" ] && PS1="N$NNNLVL $PS1"

# Random color script
bash /opt/shell-color-scripts/randomcolors.sh
