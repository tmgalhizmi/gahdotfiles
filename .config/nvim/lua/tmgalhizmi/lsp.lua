require("mason").setup()
require("mason-lspconfig").setup({
    ensure_installed = { "lua_ls", "pyright", "emmet_ls", "cssls", "tsserver" }
})

local lspconfig = require("lspconfig")

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
    -- Formatting
    if client.server_capabilities.documentFormattingProvider then
        vim.api.nvim_create_autocmd("BufWritePre", {
            group = vim.api.nvim_create_augroup("Format", { clear = true }),
            buffer = bufnr,
            callback = function() vim.lsp.buf.formatting_seq_sync() end
        })
    end
    -- Enable completion triggered by <c-x><c-o>
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- Mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local bufopts = { noremap = true, silent = true, buffer = bufnr }
    vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, bufopts)
    vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, bufopts)
    vim.keymap.set("n", "<leader>vd", function() vim.diagnostic.open_float() end, bufopts)

    vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, bufopts)
    vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, bufopts)

    vim.keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, bufopts)
    vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.references() end, bufopts)

    vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
    vim.keymap.set("n", "gi", vim.lsp.buf.implementation, bufopts)
    vim.keymap.set("n", "<leader>k", vim.lsp.buf.signature_help, bufopts)
    vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, bufopts)
    vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, bufopts)
    vim.keymap.set("n", "gr", vim.lsp.buf.references, bufopts)
    vim.keymap.set("n", "<leader>gr", function() require("telescope.builtin").lsp_references() end)
end

-- Set up completion using nvim_cmp with LSP source
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

local servers = { "lua_ls", "pyright", "emmet_ls", "cssls", "tsserver", "eslint" }

for _, lsp in ipairs(servers) do
    lspconfig[lsp].setup {
        on_attach = on_attach,
        capabilities = capabilities
    }
end

--[[ lspconfig.ccls.setup({
    capabilities = capabilities,
    on_attach = on_attach,
    init_options = {
        diagnostic = {
            onOpen = 0,
            onChange = 1,
            onSave = 0,
        },
        index = {
            comments = 1, -- index only doxygen comment markers
            onChange = true, -- force index file to be not on every save
        },
        cache = {
            directory = "", -- have to disable caching, data race are common occurences
        },
    },
}) ]]
