vim.opt.guicursor = ""

-- Better editor UI
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.numberwidth = 5

vim.opt.cursorline = true
vim.opt.colorcolumn = "80" -- highlight maximum text width

-- Better editing exprience
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.cindent = true
vim.opt.smarttab = true

vim.opt.smartindent = true

-- Soft-wrap text (only visually)
-- Source: https://stackoverflow.com/questions/36950231/auto-wrap-lines-in-vim-without-inserting-newlines
vim.opt.textwidth = 80
vim.opt.wrapmargin = 0
vim.opt.wrap = true
vim.opt.linebreak = true -- breaks by word rather than character

vim.opt.list = true
vim.opt.listchars = 'trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂'
-- vim.opt.listchars = 'eol:¬,space:·,lead: ,trail:·,nbsp:◇,tab:→-,extends:▸,precedes:◂,multispace:···⬝,leadmultispace:│   ,'

-- Makes neovim and host OS clipboard play nicely with each other
vim.opt.clipboard = 'unnamedplus'

-- Undo and backup options
vim.opt.backup = false
vim.opt.undofile = true
vim.opt.swapfile = false
vim.opt.undodir = "/tmp/"

vim.opt.hlsearch = false
vim.opt.incsearch = true

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

vim.opt.scrolloff = 8

-- Decrease update time
vim.opt.updatetime = 80

-- Better buffer splitting
vim.opt.splitright = true
vim.opt.splitbelow = true
