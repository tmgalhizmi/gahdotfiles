local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
    PACKER_BOOTSTRAP = fn.system {
        "git",
        "clone",
        "--depth",
        "1",
        "https://github.com/wbthomason/packer.nvim",
        install_path,
    }
    print "Installing packer close and reopen Neovim..."
    vim.cmd [[packadd packer.nvim]]
end

-- Automatically run :PackerCompile whenever plugins.lua is update with an autocommand
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])
-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
    return
end

-- Have packer use a popup window
packer.init({
    display = {
        open_fn = function()
            return require("packer.util").float({ border = "rounded" })
        end,
    }
})

-- Install your plugins here
packer.startup(function(use)
    -- Packer can manage itsel
    use "wbthomason/packer.nvim"

    --[[ autopairs & autotag ]]
    --
    use "windwp/nvim-autopairs"
    use {
        "windwp/nvim-ts-autotag",
        config = function()
            require("nvim-ts-autotag").setup()
        end
    }

    --[[ cmp plugins ]]
    --
    use "hrsh7th/nvim-cmp"         -- the completion plugin
    use "hrsh7th/cmp-buffer"       -- buffer completions
    use "hrsh7th/cmp-path"         -- path completions
    use "hrsh7th/cmp-cmdline"      -- cmdline completions
    use "hrsh7th/cmp-nvim-lsp"     -- source for lsp
    use "saadparwaiz1/cmp_luasnip" -- snippet completions

    --[[ snippets ]]
    --
    use "L3MON4D3/LuaSnip"             -- snippet engine
    use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

    --[[ LSP ]]
    --
    use "neovim/nvim-lspconfig"             -- enable lsp
    use "williamboman/mason.nvim"           -- simple to use language server installer
    use "williamboman/mason-lspconfig.nvim" -- simple to use language server installer

    --[[ Git ]]
    use {
        "lewis6991/gitsigns.nvim",
        -- tag = 'release' -- To use the latest release (do not use this if you run Neovim nightly or dev builds!)
        config = function()
            require("gitsigns").setup()
        end
    }

    --[[ appearance ]]
    --
    use { "ellisonleao/gruvbox.nvim" }
    use "nvim-lualine/lualine.nvim"
    use "nvim-tree/nvim-web-devicons"
    use { "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" }
    use "goolord/alpha-nvim"
    use "lukas-reineke/indent-blankline.nvim"
    use {
        "NvChad/nvim-colorizer.lua",
        config = function()
            require("colorizer").setup()
        end
    }

    -- file tree
    use {
        "nvim-tree/nvim-tree.lua",
        config = function()
            require("nvim-tree").setup()
        end
    }

    -- fuzzy finder
    use {
        "nvim-telescope/telescope.nvim", tag = "0.1.1",
        -- or                            , branch = "0.1.x",
        requires = { { "nvim-lua/plenary.nvim" } }
    }

    -- easily comment stuff
    use {
        "numToStr/Comment.nvim",
        config = function()
            require("Comment").setup()
        end
    }
end)
