require('nvim-treesitter.configs').setup {
    -- A list of parser names, or "all" (the four listed parsers should always be installed)
    ensure_installed = { "python", "html", "css", "javascript", "bash", "lua", "help" },

    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = false,

    -- Automatically install missing parsers when entering buffer
    -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
    auto_install = true,

    highlight = {
        enable = true,
        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
    },
    -- From https://github.com/whatsthatsmell/dots/
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "<CR>",
            scope_incremental = "<CR>",
            node_incremental = "<TAB>",
            node_decremental = "<S-TAB>",
        },
    },
    indent = { enable = true },
    matchup = { enable = true },
    autopairs = { enable = true },
    playground = {
        enable = true,
        disable = {},
        updatetime = 25,
        persist_queries = false,
        keybindings = {
            toggle_query_editor = "o",
            toggle_hl_groups = "i",
            toggle_injected_languages = "t",
            toggle_anonymous_nodes = "a",
            toggle_language_display = "I",
            focus_language = "f",
            unfocus_language = "F",
            update = "R",
            goto_node = "<cr>",
            show_help = "?",
        },
    },
    rainbow = {
        enable = true,
        extended_mode = true, -- Highlight also non-parentheses delimiters
        max_file_lines = 1000,
    },
    refactor = {
        smart_rename = { enable = true, keymaps = { smart_rename = "grr" } },
        highlight_definitions = { enable = true },
        navigation = {
            enable = true,
            keymaps = {
                goto_definition_lsp_fallback = "gnd",
                -- use telescope for these lists
                -- list_definitions = "gnD",
                -- list_definitions_toc = "gO",
                -- @TODOUA: figure out if I need both below
                goto_next_usage = "<a-*>",   -- is this redundant?
                goto_previous_usage = "<a-#>", -- also this one?
            },
        },
        -- highlight_current_scope = {enable = true}
    },
    textobjects = {
        lsp_interop = {
            enable = true,
            border = "none",
            peek_definition_code = {
                ["df"] = "@function.outer",
                ["dF"] = "@class.outer",
            },
        },
        move = {
            enable = true,
            set_jumps = true, -- whether to set jumps in the jumplist
            goto_next_start = {
                ["]m"] = "@function.outer",
                ["]]"] = "@call.outer",
            },
            goto_next_end = {
                ["]M"] = "@function.outer",
                ["]["] = "@call.outer",
            },
            goto_previous_start = {
                ["[m"] = "@function.outer",
                ["[["] = "@call.outer",
            },
            goto_previous_end = {
                ["[M"] = "@function.outer",
                ["[]"] = "@call.outer",
            },
        },
        select = {
            enable = true,
            lookahead = true,
            keymaps = {
                ["af"] = "@function.outer",
                ["if"] = "@function.inner",
                ["ac"] = "@call.outer",
                ["ic"] = "@call.inner",
            },
        },
        swap = {
            enable = true,
            swap_next = {
                [",a"] = "@parameter.inner",
            },
            swap_previous = {
                [",A"] = "@parameter.inner",
            },
        },
    },
    -- End from whatsthatsmell/dots
    autotag = {
        enable = true,
    }
}
