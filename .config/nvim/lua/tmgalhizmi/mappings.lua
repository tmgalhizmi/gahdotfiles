local opts = { noremap = true, silent = true }

vim.g.mapleader = " "

-- Normal --
--vim.keymap.set("n", "<leader>e", vim.cmd.Ex)
-- Nvim-Tree
vim.keymap.set("n", "<leader>e", vim.cmd.NvimTreeToggle, opts)

-- Copying the vscode behaviour of making tab splits
vim.keymap.set("n", "<C-\\>", vim.cmd.vsplit)
vim.keymap.set("n", "<A-\\>", vim.cmd.split)

vim.keymap.set("n", "J", "mzJ`z")

-- Scroll down and center cursor
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- Keep search in the middle
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Replace word in cursor
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- Give execute permission
vim.keymap.set("n", "<leader>x", "<cmd>!chmod 755 %<CR>", { silent = true })

-- Better window navigation
vim.keymap.set("n", "<C-h>", "<C-w>h", opts)
vim.keymap.set("n", "<C-j>", "<C-w>j", opts)
vim.keymap.set("n", "<C-k>", "<C-w>k", opts)
vim.keymap.set("n", "<C-l>", "<C-w>l", opts)

-- Navigate buffers
vim.keymap.set("n", "<S-l>", vim.cmd.bnext, opts)
vim.keymap.set("n", "<S-h>", vim.cmd.bprevious, opts)

local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>ff", builtin.find_files, opts)
vim.keymap.set("n", "<leader>fs", builtin.live_grep, opts)
vim.keymap.set("n", "<leader>fb", builtin.buffers, opts)
vim.keymap.set("n", "<leader>fh", builtin.help_tags, opts)

-- Visual --
-- Move line up and down in NORMAL and VISUAL modes
-- Reference: https://vim.fandom.com/wiki/Moving_lines_up_or_down
vim.keymap.set("v", "<C-j>", ":move '>+1<CR>gv=gv")
vim.keymap.set("v", "<C-k>", ":move '<-2<CR>gv=gv")

-- Visual Block --
-- Not using fist copy text after paste
vim.keymap.set("x", "<leader>p", "\"_dP")
