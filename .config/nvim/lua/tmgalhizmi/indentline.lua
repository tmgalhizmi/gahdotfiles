vim.opt.list = true
-- vim.cmd [[highlight IndentBlanklineIndent1 guifg=#8ec07c gui=nocombine]]
-- vim.cmd [[highlight IndentBlanklineIndent2 guifg=#fabd2f gui=nocombine]]
-- vim.cmd [[highlight IndentBlanklineIndent3 guifg=#b8bb26 gui=nocombine]]

require("indent_blankline").setup {
    space_char_blankline = " ",
    show_current_context = true,
    show_current_context_start = true,
    -- char_highlight_list = {
    --   "IndentBlanklineIndent1",
    --   "IndentBlanklineIndent2",
    --   "IndentBlanklineIndent3",
    -- },
}
