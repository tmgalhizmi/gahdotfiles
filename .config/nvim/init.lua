require("tmgalhizmi.plugins")
require("tmgalhizmi.mappings")
require("tmgalhizmi.settings")

-- Plugin config
require("tmgalhizmi.autopairs")
require("tmgalhizmi.alpha")
require("tmgalhizmi.cmp")
require("tmgalhizmi.gruvbox")
require("tmgalhizmi.indentline")
require("tmgalhizmi.lsp")
require("tmgalhizmi.lualine")
require("tmgalhizmi.nvimtree")
require("tmgalhizmi.telescope")
