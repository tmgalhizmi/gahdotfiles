# VI mode ( use ' fish_default_key_bindings' to toggle to default)
fish_vi_key_bindings

# Set the annoying greeting to empty
    set fish_greeting

[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc" 

# Some abbreviation
abbr -a -g g git
abbr -a -g trem transmission-remote
