#!/bin/sh
# My qtile autostart programs
# By T.M. Ghazi Al-Hizmi

xrdb -load ~/.config/X11/xresources &
hp-systray &
nextcloud &
/usr/lib/mate-polkit/polkit-mate-authentication-agent-1 &
picom &
#setxkbmap -model pc105 -layout us,ara -option grp:shifts_toggle &
#sleep 1; setxkbmap -option caps:swapescape &
/usr/bin/dunst &
udiskie -A -s &
unclutter &
#xwallpaper --zoom $HOME/.config/wall.png & 
# For non-systemd system
#picom -b --config ~/.config/picom/picom.conf &
