# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess

from libqtile.config import Key, Screen, Group, Drag, Click, ScratchPad, DropDown, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.utils import guess_terminal

from datetime import date

from typing import List  # noqa: F401

##### DEFINING SOME VARIABLES ######
mod = "mod4"
myTerm = guess_terminal()
myConfig = os.path.expanduser("~/.config")
today = date.today()

##### KEYBINDINGS #####
keys = [
    ### Basic movement
    Key([mod], "j", lazy.layout.down(), desc="Focus window bellow"),
    Key([mod], "k", lazy.layout.up(), desc="Focus window up"),
    Key([mod], "h", lazy.layout.left(), desc="Focus window left"),
    Key([mod], "l", lazy.layout.right(), desc="Focus window right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down in current stack"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up in current stack"),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window left in current stack"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window right in current stack"),

    Key([mod], "space", lazy.layout.next(), desc="Switch window focus to other pane(s) of stack"),

    Key([mod, "shift"], "space",
        lazy.layout.rotate(), lazy.layout.flip(),
        desc="Swap panes of split stack (Stack); Switch which side main pane occupies (Monad)"),

    Key([mod, "control"], "space", lazy.window.toggle_floating(), desc="Toggle window to floating or normal windows"),
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split & unsplit sides of stack"),

    Key([mod, "shift"], "c", lazy.window.kill(), desc="Close focus window"),
    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Logout from qtile"),
    Key([mod, "control"], "0", lazy.spawn("loginctl poweroff"), desc="Shutdown computer"),

    #Key([mod], "r", lazy.spawncmd(prompt='run'), desc="Start a little prompt"),
    Key([mod], "r", lazy.spawn("rofi -show run"), desc="Start Rofi Launcher"),

    Key([mod, "shift"], "b", lazy.hide_show_bar(), desc="Toggle visibility of given bar"),

    ### Window control
    Key([mod, "control"], "n", lazy.layout.normalize(), desc="Back window size to normal"),
    Key([mod, "control"], "o", lazy.layout.maximize(), desc="Maximize window size in Monad"),

    Key([mod, "control"], "j",
        lazy.layout.decrease_ratio(), # Tile
        lazy.layout.shrink(),         # MonadTall, MonadWide
        lazy.layout.grow_down(),      # Bsp, Column
        desc="Shrink window size"),
    Key([mod, "control"], "k",
        lazy.layout.increase_ratio(), # Tile
        lazy.layout.grow(),           # MonadTall, MonadWide
        lazy.layout.grow_up(),        # Bsp, Column
        desc="Grow window size"),
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window left in Bsp, Column"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window right in Bsp, Column"),

    Key([mod, "mod1"], "j", lazy.layout.flip_down(), desc="Flip window to bottom in Bsp"),
    Key([mod, "mod1"], "k", lazy.layout.flip_up(), desc="Flip window to up in Bsp"),
    Key([mod, "mod1"], "h", lazy.layout.flip_left(), desc="Flip window to left in Bsp"),
    Key([mod, "mod1"], "l", lazy.layout.flip_right(), desc="Flip window to right in Bsp"),

    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between different layouts"),
    Key([mod, "shift"], "Tab", lazy.prev_layout(), desc="Toggle between different layouts"),

    Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc="Toggle window floating"),
    Key([mod, "control"], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen"),

    ### Switch focus of monitor
    Key([mod], "s", lazy.next_screen(), desc="Move focus to the next monitor"),

    ### Volume controls
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer set Master 5%+"), desc="Raise volume up"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer set Master 5%-"), desc="Raise volume down"),
    Key([], "XF86AudioMute", lazy.spawn("amixer set Master toggle"), desc="Mute audio"),

    ### Standard application
    Key([mod], "Return", lazy.spawn(myTerm), desc="Run my preferred terminal emulator"),
    Key([mod], "e", lazy.spawn(myTerm+" -e "+myConfig+"/vifm/scripts/vifmrun"), desc="Open TUI file manager"),
    Key([mod, "shift"], "e", lazy.spawn("pcmanfm"), desc="Open GUI file manager"),
    Key(["mod1", "control"], "e", lazy.spawn("emacs"), desc="Open GNU Emacs"),
    Key([mod], "n", lazy.spawn(myTerm+" -e newsboat"), desc="Open RSS feed reader"),
    Key([mod], "m", lazy.spawn("komikku"), desc="Open up manga reader"),
    Key([mod], "w", lazy.spawn("brave"), desc="Launch Brave Browser"),
    Key([mod], "c", lazy.spawn("telegram-desktop"), desc="Telegram desktop"),
    Key([mod], "g", lazy.spawn("retroarch"), desc="Emulator bundle"),
    Key([mod], "p", lazy.spawn("keepassxc"), desc="Launch Password Manager"),
    Key([mod], "o", lazy.spawn("lowriter"), desc="Open LibreOffice Writer"),
    Key([mod, "shift"], "o", lazy.spawn("localc"), desc="Open LibreOffice Calc"),
    Key([mod, "shift"], "s", lazy.spawn("simple-scan"), desc="Open Scanner App"),
    Key([mod], "i", lazy.spawn("gimp"), desc="Launch GNU Image Manipulation Program"),
    Key([mod, "shift"], "p", lazy.spawn("PCSX2"), desc="PS2 Emulator"),
    Key([mod], "v", lazy.spawn("virt-manager"), desc="Run virt-manager"),
    Key([mod, "shift"], "v", lazy.spawn("virtualbox"), desc="Run virtualbox"),
    Key([mod], "b", lazy.spawn("firefox"), desc="Launch Firefox Web Browser"),
    Key([mod], "q", lazy.spawn("qutebrowser"), desc="Launch qutebrowser")
]

##### GROUPS #####

#group_names = [("WWW", {'layout': 'max'}),
#               ("DOC", {'layout': 'monadtall'}),
#               ("TERM", {'layout': 'monadtall'}),
#               ("FILE", {'layout': 'monadwide'}),
#               ("MEDIA", {'layout': 'monadtall'}),
#               ("GFX", {'layout': 'max'}),
#               ("CHAT", {'layout': 'max'}),
#               ("VBOX", {'layout': 'stack'})]

#group_names = [("I", {'layout': 'max'}),
#               ("II", {'layout': 'monadtall'}),
#               ("III", {'layout': 'tile'}),
#               ("IV", {'layout': 'monadwide'}),
#               ("V", {'layout': 'monadtall'}),
#               ("VI", {'layout': 'max'}),
#               ("VII", {'layout': 'max'}),
#               ("VIII", {'layout': 'columns'})]
#
#groups = [Group(name, **kwargs) for name, kwargs in group_names]
#
#for i, (name, kwargs) in enumerate(group_names, 1):
#    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
#    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

groups = [
        Group("I", layout="max", matches=[Match(wm_class=["firefox"]), Match(wm_class=["Brave-browser"])]),
        Group("II", layout="monadtall", matches=[Match(wm_class=["libreoffice-writer"]), Match(wm_class=["libreoffice-calc"])]),
        Group("III", layout="tile"), #, matches=[Match(wm_class=["Alacritty"])]),
        Group("IV", layout="monadwide", matches=[Match(title=["/home/tmgalhizmi/.config/vifm/scripts/vifmrun"]),
                                        Match(wm_class=["Pcmanfm"])]),
        Group("V", layout="monadtall", matches=[Match(wm_class=["mpv"])]),
        Group("VI", layout="max", matches=[Match(wm_class=["TelegramDesktop"])]),
        Group("VII", layout="max", matches=[Match(wm_class=["retroarch"]), Match(wm_class=["Gimp"])]),
        Group("VIII", layout="columns", matches=[Match(wm_class=["Virt-manager"]), Match(wm_class=["VirtualBox Manager"])])
        ]

grp_list = []
# Make group list with respective integers
for i in range(len(groups) + 1):
    grp_list.append(i)

# Start the integer from one to bind it later
grp_list.remove(0)

# Iterate and bind index as key
for k, v in zip(grp_list, groups):
    keys.extend([
        # mod1 + letter of group = switch to grouplazy.group[j].toscreen()
        Key([mod], str(k), lazy.group[v.name].toscreen()),

        # mod1 + shift + letter of group = switch to
        # & move focused window to group
        Key([mod, "shift"], str(k), lazy.window.togroup(v.name)),
    ])

##### LAYOUTS #####
colors = ["#002b36",            # [0] S_base03
          "#073642",            # [1] S_base02
          "#586e75",            # [2] S_base01
          "#657b83",            # [3] S_base00
          "#839496",            # [4] S_base0
          "#93a1a1",            # [5] S_base1
          "#eee8d5",            # [6] S_base2
          "#fdf6e3",            # [7] S_base3

          "#b58900",            # [8] S_yellow
          "#cb4b16",            # [9] S_orange
          "#dc322f",            # [10] S_red
          "#d33682",            # [11] S_magenta
          "#6c71c4",            # [12] S_violet
          "#268bd2",            # [13] S_blue
          "#2aa198",            # [14] S_cyan
          "#859900",            # [15] S_green
        ]
layout_theme = {"border_width": 2,
                "margin": 5,
                "border_focus": colors[10],
                "border_normal": colors[2]
}

layouts = [
    layout.Max(),
    #layout.Stack(**layout_theme, num_stacks=2),
    layout.Columns(**layout_theme, border_focus_stack=colors[14]),
    layout.MonadTall(**layout_theme, single_border_width=0),
    layout.MonadWide(**layout_theme, single_border_width=0),
    layout.Tile(**layout_theme),
    layout.Floating(border_normal=colors[4], border_focus=colors[8]),
    layout.Matrix(columns=2, **layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.TreeTab(),
    layout.Zoomy()
]

##### SCREENS #####
widget_defaults = dict(
    font="UbuntuMono Nerd Font",
    fontsize=12,
    padding=2,
    background=colors[1]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
             widget.TextBox("["),
             widget.CurrentLayout(padding=5,
                     foreground=colors[9],
                     background=colors[0]
                     ),
             widget.TextBox("]"),
             #widget.Image(filename="~/.config/qtile/square.png"),
             #widget.Image(filename="~/.config/qtile/square1.png"),
             widget.GroupBox(highlight_method="line",
                 #borderwidth=3,
                 #block_highlight_text_color=colors[1], # for 'block' highlight
                 highlight_color=[colors[3], colors[1]], # for 'line' highlight
                 #hide_unused=True,
                 margin_x=1,
                 margin_y=2,
                 padding_x=3,
                 padding_y=4,
                 rounded=True,
                 disable_drag=True,
                 inactive=colors[4],
                 active=colors[7],
                 background=colors[1],
                 this_current_screen_border=colors[10]
                 ),
             #widget.Image(filename="~/.config/qtile/square2.png"),
             widget.Prompt(),
             widget.WindowName(foreground=colors[9]),
             # Failed, check on option
             #widget.KeyboardLayout(background=[4],
             #    configured_keyboards=["us", "ara"],
             #    option="caps:swapescape,grp:shifts_toggle"),
             widget.Image(filename="~/.config/qtile/square3.png"),
             widget.TextBox(
                     text="📦",
                     padding = 5,
                     foreground=colors[1],
                     background=colors[11],
                     fontsize=14
                     ),
             widget.CheckUpdates(
                     update_interval=1800,
                     distro="Arch_checkupdates",
                     display_format="{updates} Updates",
                     foreground=colors[7],
                     background=colors[11]
                     ),
             widget.Image(filename="~/.config/qtile/square4.png"),
             #widget.TextBox(
             #        text="📡",
             #        padding = 3,
             #        foreground=colors[6],
             #        background=colors[2],
             #        fontsize=13
             #        ),
             widget.Net(interface="eth0",
                     format=' {down}  {up}',
                     foreground=colors[6],
                     background=colors[2]
                 ),
             widget.Image(filename="~/.config/qtile/square5.png"),
             #widget.TextBox(
             #        text="☵",
             #        padding = 3,
             #        foreground=colors[0],
             #        background=colors[14],
             #        fontsize=21
             #        ),
             widget.TextBox(
                     text="🧠",
                     padding = 3,
                     foreground=colors[0],
                     background=colors[14],
                     fontsize=14
                     ),
             widget.Memory(update_interval=7.0,
                     forground=colors[0],
                     background=colors[14]),
             widget.Image(filename="~/.config/qtile/square6.png"),
             widget.Image(filename="~/.config/qtile/square7.png"),
             widget.TextBox(
                     text=" 🔊",
                     foreground=colors[0],
                     background=colors[13],
                     padding = 0,
                     fontsize=13
                     ),
             widget.Volume(
                     foreground=colors[0],
                     background=colors[13],
                     padding = 5
                     ),
             widget.Image(filename="~/.config/qtile/square8.png"),
             widget.Wallpaper(directory='~/pix/Wallpapers/wallpapers-master',
                      random_selection=True,
                      wallpaper_command=['xwallpaper', '--zoom']),
             widget.TextBox(text=" 🕒",
                 background=colors[1],
                 padding = 5,
                 fontsize=13,
                 mouse_callbacks={'Button1': lambda qtile: qtile.cmd_spawn(os.path.expanduser("~/.local/bin/bar/date.sh"))}
                 ),
             widget.Clock(background=colors[1],
                 format="%a, %d-%b-%Y  [ %R ]"
                 ),
             widget.Systray(background=colors[1], padding=7, icon_size=17)
              ],
            23
        ),
    ),
    Screen(
        top=bar.Bar(
            [
             widget.TextBox("["),
             widget.CurrentLayout(padding=5,
                     foreground=colors[9],
                     background=colors[0]
                     ),
             widget.TextBox("]"),
             #widget.Image(filename="~/.config/qtile/square.png"),
             #widget.Image(filename="~/.config/qtile/square1.png"),
             widget.GroupBox(highlight_method="line",
                 #borderwidth=3,
                 #block_highlight_text_color=colors[1], # for 'block' highlight
                 highlight_color=[colors[3], colors[1]], # for 'line' highlight
                 #hide_unused=True,
                 margin_x=1,
                 margin_y=2,
                 padding_x=3,
                 padding_y=4,
                 rounded=True,
                 disable_drag=True,
                 inactive=colors[4],
                 active=colors[7],
                 background=colors[1],
                 this_current_screen_border=colors[10]
                 ),
             #widget.Image(filename="~/.config/qtile/square2.png"),
             widget.Prompt(),
             widget.WindowName(foreground=colors[9]),
             # Failed, check on option
             #widget.KeyboardLayout(background=[4],
             #    configured_keyboards=["us", "ara"],
             #    option="caps:swapescape,grp:shifts_toggle"),
             widget.Image(filename="~/.config/qtile/square3.png"),
             widget.TextBox(
                     text="📦",
                     padding = 5,
                     foreground=colors[1],
                     background=colors[11],
                     fontsize=14
                     ),
             widget.CheckUpdates(
                     update_interval=1800,
                     distro="Arch_checkupdates",
                     display_format="{updates} Updates",
                     foreground=colors[7],
                     background=colors[11]
                     ),
             widget.Image(filename="~/.config/qtile/square4.png"),
             #widget.TextBox(
             #        text="📡",
             #        padding = 3,
             #        foreground=colors[6],
             #        background=colors[2],
             #        fontsize=13
             #        ),
             widget.Net(interface="eth0",
                     format=' {down}  {up}',
                     foreground=colors[6],
                     background=colors[2]
                 ),
             widget.Image(filename="~/.config/qtile/square5.png"),
             #widget.TextBox(
             #        text="☵",
             #        padding = 3,
             #        foreground=colors[0],
             #        background=colors[14],
             #        fontsize=21
             #        ),
             widget.TextBox(
                     text="🧠",
                     padding = 3,
                     foreground=colors[0],
                     background=colors[14],
                     fontsize=14
                     ),
             widget.Memory(update_interval=7.0,
                     forground=colors[0],
                     background=colors[14]),
             widget.Image(filename="~/.config/qtile/square6.png"),
             widget.Image(filename="~/.config/qtile/square7.png"),
             widget.TextBox(
                     text=" 🔊",
                     foreground=colors[0],
                     background=colors[13],
                     padding = 0,
                     fontsize=13
                     ),
             widget.Volume(
                     foreground=colors[0],
                     background=colors[13],
                     padding = 5
                     ),
             widget.Image(filename="~/.config/qtile/square8.png"),
             widget.Wallpaper(directory='~/pix/Wallpapers/wallpapers-master',
                      random_selection=True,
                      wallpaper_command=['xwallpaper', '--zoom']),
             widget.TextBox(text=" 🕒",
                 background=colors[1],
                 padding = 5,
                 fontsize=13,
                 mouse_callbacks={'Button1': lambda qtile: qtile.cmd_spawn(os.path.expanduser("~/.local/bin/bar/date.sh"))}
                 ),
             widget.Clock(background=colors[1],
                 format="%a, %d-%b-%Y  [ %R ]"
                 ),
             widget.Systray(background=colors[1], padding=7, icon_size=17)
              ],
            23
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
], **layout_theme)
auto_fullscreen = True
focus_on_window_activation = "smart"

# Autostart Applications
@hook.subscribe.startup_once
def autostart ():
    autostart = myConfig + "/qtile/autostart.sh"
    subprocess.Popen([autostart])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
