-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Load additional library by lcpz
local lain = require("lain")
local freedesktop = require("freedesktop")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function(err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({
            preset = naughty.config.presets.critical,
            title = "Oops, an error happened!",
            text = tostring(err)
        })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(gears.filesystem.get_configuration_dir() .. "themes/default/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "alacritty"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor
webbrowser = "qutebrowser"
filemanager = "thunar"
fileman_cli = terminal .. " -e ranger"

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,       -- 1
    awful.layout.suit.tile,           -- 2
    awful.layout.suit.tile.bottom,    -- 3
    awful.layout.suit.max,            -- 4
    awful.layout.suit.max.fullscreen, -- 5
    awful.layout.suit.magnifier,      -- 6
    -- awful.layout.suit.tile.left,        -- 7
    -- awful.layout.suit.tile.top,         -- 8
    -- awful.layout.suit.tile.bottom,      -- 8
    -- awful.layout.suit.spiral,           -- 9
    -- awful.layout.suit.spiral.dwindle,   -- 10
    -- awful.layout.suit.fair,             -- 11
    -- awful.layout.suit.fair.horizontal,  -- 12
    -- awful.layout.suit.corner.nw,        -- 13
    -- awful.layout.suit.corner.ne,        -- 14
    -- awful.layout.suit.corner.sw,        -- 15
    -- awful.layout.suit.corner.se,        -- 16
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
    { "hotkeys",     function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
    { "manual",      terminal .. " -e man awesome" },
    { "edit config", editor_cmd .. " " .. awesome.conffile },
    { "lockscreen",  function() awful.spawn.with_shell("~/.local/bin/lock-and-blur.sh") end },
    { "restart",     awesome.restart },
    { "quit",        function() awesome.quit() end },
    { "reboot",      function() awful.spawn("loginctl reboot") end },
    { "poweroff",    function() awful.spawn("loginctl poweroff") end },
}

-- Change to freedestop menu by lcpz
mymainmenu = freedesktop.menu.build({
    before = {
        { "Awesome", myawesomemenu, beautiful.awesome_icon }
    },
    after = {
        { "Open terminal", terminal },
        -- other triads can be put here
    }
})

mylauncher = awful.widget.launcher({
    image = beautiful.awesome_icon,
    menu = mymainmenu
})

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create lain widget
local markup = lain.util.markup
local gray = "#94928F"

local volume = lain.widget.alsa({
    timeout = 1,
    settings = function()
        header = "Vol "
        vlevel = volume_now.level

        if volume_now.status == "off" then
            vlevel = vlevel .. "M "
        else
            vlevel = vlevel .. "% "
        end
        -- markup() == markup.fg.color()
        widget:set_markup(markup.font(beautiful.font, markup(gray, header) .. vlevel))
    end
})

local mem = lain.widget.mem({
    timeout = 7,
    settings = function()
        widget:set_markup(markup.font(beautiful.font, markup(gray, "Mem ") .. mem_now.used .. "Mib "))
    end
})

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
    awful.button({}, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({}, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({}, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = gears.table.join(
    awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal(
                "request::activate",
                "tasklist",
                { raise = true }
            )
        end
    end),
    awful.button({}, 3, function()
        awful.menu.client_list({ theme = { width = 250 } })
    end),
    awful.button({}, 4, function()
        awful.client.focus.byidx(1)
    end),
    awful.button({}, 5, function()
        awful.client.focus.byidx(-1)
    end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.

    -- Different layout for each tag
    local names = { " www ", " doc ", " term ", " file ", " vid ", " gfx ", " chat ", " vm " }
    local l = awful.layout.layouts
    awful.tag(names, s, { l[2], l[3], l[2], l[2], l[5], l[4], l[2], l[1] })

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
        awful.button({}, 1, function() awful.layout.inc(1) end),
        awful.button({}, 3, function() awful.layout.inc(-1) end),
        awful.button({}, 4, function() awful.layout.inc(1) end),
        awful.button({}, 5, function() awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        -- Only show tag(s) with client
        -- https://www.reddit.com/r/awesomewm/comments/dhvq8c/hide_tags_when_not_used/
        filter  = function(t) return t.selected or #t:clients() > 0 end,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        {
            -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mylayoutbox,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        {
            -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            mem,
            volume,
            mytextclock,
            wibox.widget.systray(),
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
-- awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({}, 4, awful.tag.viewnext),
    awful.button({}, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey, "Control" }, "s", hotkeys_popup.show_help,
        { description = "show help", group = "awesome" }),
    awful.key({ modkey, }, "Left", awful.tag.viewprev,
        { description = "view previous", group = "tag" }),
    awful.key({ modkey, }, "Right", awful.tag.viewnext,
        { description = "view next", group = "tag" }),
    awful.key({ modkey, }, "Escape", awful.tag.history.restore,
        { description = "go back", group = "tag" }),

    awful.key({ modkey, }, "j",
        function()
            awful.client.focus.byidx(1)
        end,
        { description = "focus next by index", group = "client" }
    ),
    awful.key({ modkey, }, "k",
        function()
            awful.client.focus.byidx(-1)
        end,
        { description = "focus previous by index", group = "client" }
    ),
    awful.key({ "Shift" }, "Escape", function() mymainmenu:show() end,
        { description = "show main menu", group = "awesome" }),

    -- Layout manipulation
    awful.key({ modkey, "Shift" }, "j", function() awful.client.swap.byidx(1) end,
        { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, "Shift" }, "k", function() awful.client.swap.byidx(-1) end,
        { description = "swap with previous client by index", group = "client" }),
    awful.key({ modkey, "Control" }, "j", function() awful.screen.focus_relative(1) end,
        { description = "focus the next screen", group = "screen" }),
    awful.key({ modkey, "Control" }, "k", function() awful.screen.focus_relative(-1) end,
        { description = "focus the previous screen", group = "screen" }),
    awful.key({ modkey, }, "u", awful.client.urgent.jumpto,
        { description = "jump to urgent client", group = "client" }),
    awful.key({ modkey, }, "Tab",
        function()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        { description = "go back", group = "client" }),

    -- Standard program
    awful.key({ modkey, }, "Return", function() awful.spawn(terminal) end,
        { description = "open a terminal", group = "launcher" }),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
        { description = "reload awesome", group = "awesome" }),
    awful.key({ modkey, "Control" }, "q", awesome.quit,
        { description = "quit awesome", group = "awesome" }),
    awful.key({ modkey, }, "l", function() awful.tag.incmwfact(0.05) end,
        { description = "increase master width factor", group = "layout" }),
    awful.key({ modkey, }, "h", function() awful.tag.incmwfact(-0.05) end,
        { description = "decrease master width factor", group = "layout" }),
    awful.key({ modkey, "Shift" }, "h", function() awful.tag.incnmaster(1, nil, true) end,
        { description = "increase the number of master clients", group = "layout" }),
    awful.key({ "Mod1", "Shift" }, "h", function() awful.tag.incnmaster(1, nil, true) end,
        { description = "increase the number of master clients", group = "layout" }),
    awful.key({ "Mod1", "Shift" }, "l", function() awful.tag.incnmaster(-1, nil, true) end,
        { description = "decrease the number of master clients", group = "layout" }),
    awful.key({ modkey, "Control" }, "h", function() awful.tag.incncol(1, nil, true) end,
        { description = "increase the number of columns", group = "layout" }),
    awful.key({ modkey, "Control" }, "l", function() awful.tag.incncol(-1, nil, true) end,
        { description = "decrease the number of columns", group = "layout" }),
    awful.key({ modkey, }, "space", function() awful.layout.inc(1) end,
        { description = "select next", group = "layout" }),
    awful.key({ modkey, "Shift" }, "space", function() awful.layout.inc(-1) end,
        { description = "select previous", group = "layout" }),

    -- Volume control
    -- Using pactl
    awful.key({}, "XF86AudioRaiseVolume", function() awful.spawn("pactl -- set-sink-volume @DEFAULT_SINK@ +3%") end,
        { description = "volume up", group = "hotkeys" }),
    awful.key({}, "XF86AudioLowerVolume", function() awful.spawn("pactl -- set-sink-volume @DEFAULT_SINK@ -3%") end,
        { description = "volume down", group = "hotkeys" }),
    awful.key({}, "XF86AudioMute", function() awful.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle") end,
        { description = "mute audio", group = "hotkeys" }),
    -- Using amixer and pipewire voidlinux
    --[[ awful.key({ }, "XF86AudioRaiseVolume", function () awful.spawn("amixer -c 1 set Master 5%+") end,
              {description = "volume up", group = "hotkeys"}),
    awful.key({ }, "XF86AudioLowerVolume", function () awful.spawn("amixer -c 1 set Master 5%-") end,
              {description = "volume down", group = "hotkeys"}),
    awful.key({ }, "XF86AudioMute", function () awful.spawn("amixer -c 1 set Master toggle") end,
              {description = "mute audio", group = "hotkeys"}),  ]]
    -- Using amixer and pipewire Slackware
    --[[ awful.key({ }, "XF86AudioRaiseVolume",
			function ()
				awful.spawn(string.format("%s set %s 3%%+", volume.cmd, volume.channel))
				volume.update()
			end,
              {description = "volume up", group = "hotkeys"}),
    awful.key({ }, "XF86AudioLowerVolume",
			function ()
				awful.spawn(string.format("%s set %s 3%%-", volume.cmd, volume.channel))
				volume.update()
			end,
              {description = "volume down", group = "hotkeys"}),
    awful.key({ }, "XF86AudioMute",
			function ()
				awful.spawn(string.format("%s set %s toggle", volume.cmd, volume.channel))
				volume.update()
			end,
              {description = "mute audio", group = "hotkeys"}), ]]

    --[[ awful.key({ }, "XF86MonBrightnessUp", function () awful.spawn("light -As 'sysfs/backlight/intel_backlight' 5") end,
              {description = "brightness up", group = "hotkeys"}),
    awful.key({ }, "XF86MonBrightnessDown", function () awful.spawn("light -Us 'sysfs/backlight/intel_backlight'  5") end,
              {description = "brightness down", group = "hotkeys"}), ]]

    -- Screenshoot control
    awful.key({ modkey }, "Print", function() awful.spawn("screeenshooter.sh -sns") end,
        { description = "screeenshoot without shadow", group = "hotkeys" }),
    awful.key({}, "Print", function() awful.spawn("screeenshooter.sh -f") end,
        { description = "screeenshoot entire screen", group = "hotkeys" }),
    awful.key({ "Mod1" }, "Print", function() awful.spawn("screeenshooter.sh -w") end,
        { description = "screeenshoot active window", group = "hotkeys" }),
    awful.key({ "Shift" }, "Print", function() awful.spawn("screeenshooter.sh -s") end,
        { description = "screenshoot with selection", group = "hotkeys" }),
    -- Copy screenshoot to clipboard
    awful.key({ "Control" }, "Print", function() awful.spawn("screeenshooter.sh -fc") end,
        { description = "screeenshoot screen to clipboard", group = "hotkeys" }),
    awful.key({ "Mod1", "Control" }, "Print", function() awful.spawn("screeenshooter.sh -wc") end,
        { description = "screeenshoot active window to clipboard", group = "hotkeys" }),
    awful.key({ "Shift", "Control" }, "Print", function() awful.spawn("screeenshooter.sh -sc") end,
        { description = "screenshoot with selection to clipboard", group = "hotkeys" }),

    -- User programs
    awful.key({ modkey }, "w", function() awful.spawn(webbrowser) end,
        { description = "Web Browser", group = "applications" }),
    awful.key({ modkey, "Shift" }, "w", function() awful.spawn("firefox") end,
        { description = "Firefox", group = "applications" }),
    awful.key({ modkey, "Shift" }, "b", function() awful.spawn("seamonkey") end,
        { description = "Seamonkey", group = "applications" }),
    awful.key({ modkey }, "o", function() awful.spawn("libreoffice") end,
        { description = "LibreOffice", group = "applications" }),
    awful.key({ modkey, "Shift" }, "o", function() awful.spawn("onlyoffice-desktopeditors") end,
        { description = "OnlyOffice", group = "applications" }),
    awful.key({ modkey }, "i", function() awful.spawn("gimp") end,
        { description = "Gimp", group = "applications" }),
    awful.key({ modkey, "Shift" }, "i", function() awful.spawn("inkscape") end,
        { description = "Inkscape", group = "applications" }),
    awful.key({ modkey }, "c", function() awful.spawn("Telegram") end,
        { description = "Telegram Desktop", group = "applications" }),
    awful.key({ modkey, }, "g", function() awful.spawn("retroarch") end,
        { description = "Retroarch", group = "applications" }),
    awful.key({ modkey, "Shift" }, "n", function() awful.spawn(terminal .. " -e newsboat") end,
        { description = "RSS feed", group = "applications" }),
    awful.key({ modkey, }, "v", function() awful.spawn("virt-manager") end,
        { description = "virt manager", group = "applications" }),
    awful.key({ modkey, "Shift" }, "v", function() awful.spawn("virtualbox") end,
        { description = "VirtualBox", group = "applications" }),
    awful.key({ modkey, }, "e", function() awful.spawn(filemanager) end,
        { description = "Filemanager", group = "applications" }),
    awful.key({ modkey, }, "s", function() awful.spawn("simple-scan") end,
        { description = "Scan files", group = "applications" }),
    awful.key({ modkey, "Control" }, "9", function() awful.spawn("loginctl reboot") end,
        { description = "reboot computer", group = "launcher" }),
    awful.key({ modkey, "Control" }, "0", function() awful.spawn("loginctl poweroff") end,
        { description = "shutdown computer", group = "launcher" }),
    awful.key({ modkey, "Control" }, "n",
        function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal(
                    "request::activate", "key.unminimize", { raise = true }
                )
            end
        end,
        { description = "restore minimized", group = "client" }),

    -- Prompt
    awful.key({ modkey }, "r", function() awful.screen.focused().mypromptbox:run() end,
        { description = "run prompt", group = "launcher" }),
    awful.key({ modkey, "Shift" }, "Return", function() awful.spawn("rofi -show run") end,
        { description = "Launch Rofi", group = "launcher" }),
    awful.key({ "Mod1", "Control" }, "l", function() awful.spawn.with_shell("~/.local/bin/lock-and-blur.sh") end,
        { description = "Lockscreen", group = "launcher" }),

    awful.key({ modkey }, "x",
        function()
            awful.prompt.run {
                prompt       = "Run Lua code: ",
                textbox      = awful.screen.focused().mypromptbox.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir() .. "/history_eval"
            }
        end,
        { description = "lua execute prompt", group = "awesome" }),

    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
        { description = "show the menubar", group = "launcher" }),

    -- Toggle Wibox
    awful.key({ modkey }, "b", function()
            for s in screen do
                s.mywibox.visible = not s.mywibox.visible
                if s.mybottomwibox then
                    s.mybottomwibox.visible = not s.mybottomwibox.visible
                end
            end
        end,
        { description = "toggle wibox", group = "awesome" })
)

clientkeys = gears.table.join(
-- Window focus and manipulation
--[[awful.key({ modkey,            }, "h", function (c)
        awful.client.focus.bydirection("left")
        c:lower()
    end,
    {description = "focus left window", group = "client"}),
    awful.key({ modkey,            }, "j", function (c)
        awful.client.focus.bydirection("down")
        c:lower()
    end,
    {description = "focus down window", group = "client"}),
    awful.key({ modkey,            }, "k", function (c)
        awful.client.focus.bydirection("up")
        c:lower()
    end,
    {description = "focus up window", group = "client"}),
    awful.key({ modkey,            }, "l", function (c)
        awful.client.focus.bydirection("right")
        c:lower()
    end,
    {description = "focus right window", group = "client"}),

    awful.key({ modkey, "Shift"    }, "h", function (c)
        awful.client.swap.bydirection("left")
        c:raise()
    end,
    {description = "swap with left window", group = "client"}),
    awful.key({ modkey, "Shift"    }, "j", function (c)
        awful.client.swap.bydirection("down")
        c:raise()
    end,
    {description = "swap with down window", group = "client"}),
    awful.key({ modkey, "Shift"    }, "k", function (c)
        awful.client.swap.bydirection("up")
        c:raise()
    end,
    {description = "swap with up window", group = "client"}),
    awful.key({ modkey, "Shift"    }, "l", function (c)
        awful.client.swap.bydirection("right")
        c:raise()
    end,
    {description = "swap with right window", group = "client"}),]]

-- Floting window
    awful.key({ modkey, "Mod1" }, "y", function(c)
            if c.floating then
                c:relative_move(-10, 0, 0, 0)
            end
        end,
        { description = "move floating window left", group = "client" }),
    awful.key({ modkey, "Mod1" }, "i", function(c)
            if c.floating then
                c:relative_move(0, -10, 0, 0)
            end
        end,
        { description = "move floating window up", group = "client" }),
    awful.key({ modkey, "Mod1" }, "u", function(c)
            if c.floating then
                c:relative_move(0, 10, 0, 0)
            end
        end,
        { description = "move floating window down", group = "client" }),
    awful.key({ modkey, "Mod1" }, "o", function(c)
            if c.floating then
                c:relative_move(10, 0, 0, 0)
            end
        end,
        { description = "move floating window right", group = "client" }),
    awful.key({ modkey, "Control" }, "y", function(c)
            if c.floating then
                c:relative_move(0, 0, -10, 0)
            else
                awful.tag.incmwfact(-0.05)
            end
        end,
        {
            description = "increase floating window width, increase master width factor",
            group = "client",
            group = "layout"
        }),
    awful.key({ modkey, "Control" }, "i", function(c)
            if c.floating then
                c:relative_move(0, 0, 0, -10)
            end
        end,
        { description = "decrease floating window height", group = "client" }),
    awful.key({ modkey, "Control" }, "u", function(c)
            if c.floating then
                c:relative_move(0, 0, 0, 10)
            end
        end,
        { description = "increase floating window height", group = "client" }),
    awful.key({ modkey, "Control" }, "o", function(c)
            if c.floating then
                c:relative_move(0, 0, 10, 0)
            else
                awful.tag.incmwfact(0.05)
            end
        end,
        {
            description = "decrease floating window width, decrease master width factor",
            group = "client",
            group = "layout"
        }),

    awful.key({ modkey, }, "f",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        { description = "toggle fullscreen", group = "client" }),
    awful.key({ modkey, "Shift" }, "c", function(c) c:kill() end,
        { description = "close", group = "client" }),
    awful.key({ modkey, "Control" }, "space", awful.client.floating.toggle,
        { description = "toggle floating", group = "client" }),
    awful.key({ modkey, "Control" }, "Return", function(c) c:swap(awful.client.getmaster()) end,
        { description = "move to master", group = "client" }),
    awful.key({ modkey, "Shift" }, "o", function(c) c:move_to_screen() end,
        { description = "move to screen", group = "client" }),
    awful.key({ modkey, }, "t", function(c) c.ontop = not c.ontop end,
        { description = "toggle keep on top", group = "client" }),
    awful.key({ modkey, "Control" }, "t", function(c) c.sticky = not c.sticky end,
        { description = "toggle sticky", group = "client" }),
    awful.key({ modkey, }, "n",
        function(c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end,
        { description = "minimize", group = "client" }),
    awful.key({ modkey, }, "m",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end,
        { description = "(un)maximize", group = "client" }),
    awful.key({ modkey, "Control" }, "m",
        function(c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end,
        { description = "(un)maximize vertically", group = "client" }),
    awful.key({ modkey, "Shift" }, "m",
        function(c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end,
        { description = "(un)maximize horizontally", group = "client" }),

    -- Toggle titlebar
    awful.key({ modkey, "Mod1" }, "t", awful.titlebar.toggle,
        { description = "toggle titlebar", group = "client" })

)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            { description = "view tag #" .. i, group = "tag" }),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            { description = "toggle tag #" .. i, group = "tag" }),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
            { description = "move focused client to tag #" .. i, group = "tag" }),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            { description = "toggle focused client on tag #" .. i, group = "tag" })
    )
end

clientbuttons = gears.table.join(
    awful.button({}, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey }, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen
        }
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {
                "DTA",   -- Firefox addon DownThemAll.
                "copyq", -- Includes session name in class.
                "pinentry",
            },
            class = {
                "Arandr",
                "Blueman-manager",
                "Gpick",
                "Kruler",
                "MessageWin",  -- kalarm.
                "Nsxiv",
                "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                "Wpa_gui",
                "veromix",
                "xtightvncviewer",
                "Xsane",
                "Onboard",
                "KeePassXC" },
            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name = {
                "Event Tester", -- xev.
            },
            role = {
                "AlarmWindow",   -- Thunderbird's calendar.
                "ConfigManager", -- Thunderbird's about:config.
                "pop-up",        -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = { floating = true }
    },

    -- Add titlebars to normal clients and dialogs
    {
        rule_any = { type = { "normal", "dialog" }
        },
        properties = { titlebars_enabled = false }
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    {
        rule = { class = "qutebrowser" },
        properties = { screen = 1, tag = " www " }
    },
    {
        rule = { class = "SeaMonkey" },
        properties = { screen = 1, tag = " www " }
    },
    {
        rule = { class = "libreoffice" },
        properties = { screen = 1, tag = " doc " }
    },
    {
        rule = { class = "soffice" },
        properties = { screen = 1, tag = " doc " }
    },
    {
        rule = { class = "Thunar" },
        properties = { screen = 1, tag = " file " }
    },
    {
        rule = { class = "Simple-scan" },
        properties = { screen = 1, tag = " vid " }
    },
    {
        rule = { class = "Anydesk" },
        properties = { screen = 1, tag = " vid " }
    },
    {
        rule = { class = "Inkscape" },
        properties = { screen = 1, tag = " gfx " }
    },
    {
        rule = { class = "Gimp" },
        properties = { screen = 1, tag = " gfx " }
    },
    {
        rule = { class = "Firefox" },
        properties = { screen = 1, tag = " chat " }
    },
    {
        rule = { class = "TelegramDesktop" },
        properties = { screen = 1, tag = " chat " }
    },
    {
        rule = { class = "Virt-manager" },
        properties = { screen = 1, tag = " vm " }
    },
    {
        rule = { class = "VirtualBox Manager" },
        properties = { screen = 1, tag = " vm " }
    },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
        and not c.size_hints.user_position
        and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({}, 1, function()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.move(c)
        end),
        awful.button({}, 3, function()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c):setup {
        {
            -- Left
            awful.titlebar.widget.closebutton(c),
            awful.titlebar.widget.floatingbutton(c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton(c),
            awful.titlebar.widget.ontopbutton(c),
            layout = wibox.layout.fixed.horizontal()
        },
        {
            -- Middle
            {
                -- Title
                align  = "right",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        {
            -- Right
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", { raise = true })
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- Autostart program
awful.spawn.single_instance("pipewire", awful.rules.rules)
awful.spawn.single_instance("setxkbmap -model pc105 -layout us,ara -option grp:shifts_toggle", awful.rules.rules)
--awful.spawn.single_instance("setxkbmap -model pc105 -layout us,us,ara -variant ,workman, -option grp:shifts_toggle", awful.rules.rules)
awful.spawn.single_instance("xcompmgr -fF -I-.004 -O-.004 -D2 -cC -t-4 -l-6 -r5", awful.rules.rules)
awful.spawn.single_instance("nextcloud", awful.rules.rules)
awful.spawn.single_instance("locker.sh", awful.rules.rules)
awful.spawn.single_instance("hp-systray", awful.rules.rules)
awful.spawn.single_instance("udiskie -A -t", awful.rules.rules)
awful.spawn.single_instance("unclutter", awful.rules.rules)
