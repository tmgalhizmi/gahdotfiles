#!/bin/bash
# By T.M. Ghazi Al-Hizmi 2022
#
# This script automatically download package
# according to <package>.info file
#
# How to use:
#   sbo-dl.sh /path/to/file.info
#
# Change the path to where info file is
# or you can pass only directory where
# info file is saved.

# Map our current directory
cwd=$(pwd)

# This funtion is needed for downloading the tarball
download_pkg() {
    for url in $1; do
        wget -c --content-disposition $url
    done
}

# Check if argument is info file or directory
if [ -f $1 ]; then
    cd $(dirname $1)
    source $(basename $1).info
elif [ -d $1 ]; then
    cd $1
    source $(basename $1).info
else
    echo 'info file not found'
fi

# Problem if DOWNLOAD have multiple links
#eval $(grep "DOWNLOAD=" *.info)
#eval $(grep "DOWNLOAD_x86_64=" *.info)

# Download source tarballs
[ -z ${DOWNLOAD[@]} ] || for url in ${DOWNLOAD[@]}; do download_pkg $url; done
[ -z ${DOWNLOAD_x86_64[@]} ] || for url in ${DOWNLOAD_x86_64[@]}; do download_pkg $url; done

# Check md5sum
#md5sum -c <(echo $MD5SUM $PRGNAM-$VERSION.tar.?z?)

# Unset variables from info file
infovar=(PRGNAM VERSION HOMEPAGE DOWNLOAD MD5SUM DOWNLOAD_x86_64 MD5SUM_x86_64
    REQUIRES MAINTAINER EMAIL)

for v in ${infovar[*]}; do
    unset $v
done

cd $cwd
