#!/bin/bash
# Toggle PipeWire audio output
# To list all sinks, run:
#   pactl list short sinks
speaker="alsa_output.pci-0000_38_00.6.analog-stereo"
monitor="alsa_output.pci-0000_38_00.1.hdmi-stereo"
default_sink=$(pactl get-default-sink)

# Toggle audio output
if [ $default_sink = $speaker ]
then
    pactl set-default-sink $monitor
else
    pactl set-default-sink $speaker
fi
