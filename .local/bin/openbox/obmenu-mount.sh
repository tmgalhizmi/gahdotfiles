#!/bin/sh
# Openbox dynamic menu to mount usb drive
# By T.M. Ghazi Al-Hizmi

echo '<openbox_pipe_menu>'
usbstatus=$( udisksctl status )
#usbpath=$( lsblk -rpo "name,type,size,mountpoint" | awk '$2=="part"&&$4==""{print $1}' )
#usbstatus=$( lsblk -rpo "name,type,size,mountpoint" | awk '$2=="part"&&$4==""{print $4}' | sed 's/^$/unmounted/' )
    
echo "<item label=\"$usbstatus\"/>"
#echo "<item label=\"$usbstatus\"/>"
echo '</openbox_pipe_menu>'
