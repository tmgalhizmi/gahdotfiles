#!/bin/bash
# Search specific package install with pkgtool
# and show its detail

# Assign packages into array
PKGTOOLDIR="/var/lib/pkgtools/packages/"
SCRIPT=$(basename $0)

usage() {
  echo "Usage: $SCRIPT [options] PACKAGE"
  echo ""
  echo "  Options:"
  echo "  -d  show package details"
  echo "  -c  show package contents"
  echo ""
}

bad_usage() {
  local message="$1"
  echo "For an overview of the command, execute:"
  echo "$SCRIPT -h, --help"
  echo ""

  [ $message ] && echo "$message"
}

details() {
  echo -e "\nWhich package you want to see the details!"
  echo "Insert Number:"
  read num
  less ${packages[$num]}
}

search_content() {
  local APP=$1
  grep "$APP" "$PKGTOOLDIR"*
}

list_package() {
  local APP=$1
  for pkg in $APP
  do
    packages=( $(find $PKGTOOLDIR -iname "*$pkg*") )
    if [ -z $packages ]; then
      echo -e "$pkg: package not found\n"
    else
      echo -e "$pkg: found ${#packages[@]} package(s)\n"

      for i in ${!packages[@]}
      do
        pkgnam=$(basename ${packages[$i]})
        echo "$i: $pkgnam"
      done
      echo ""
    fi
  done
}

for arg in $@; do
  case $arg in
    -d) shift; list_package $1 && details ;;
    -c) shift; search_content $1 ;;
    -h) usage ;;
    *) list_package $arg ;;
  esac
done
