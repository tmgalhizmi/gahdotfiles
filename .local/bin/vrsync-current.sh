#!/bin/bash
# Automatically rsync vim; add clipboard support and urxvt
SLACK_VERSION=${SLACK_VERSION:-slackware64-current}
LINK="rsync://ftp.osuosl.org/slackware/$SLACK_VERSION/source/"
CMD="rsync -avP --delete-after"

cd ~/slackware/dev/
case $1 in
    vim )
        $CMD $LINK/ap/vim .
        sed -i 's|\(TMP:-/tmp\)|\1/SBo|' vim/vim.SlackBuild
        sed -i 's/config_vim --without-x/config_vim --with-x/' vim/vim.SlackBuild
    ;;
    * )
        echo "This package out of scope"
        echo "Right now only for vim"
        exit 1
esac
