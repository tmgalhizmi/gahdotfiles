#!/bin/sh
# Script untuk memasang dan melepaskan perangkat usb

[ -z "$1" ] && echo "Input device name after this command.
Use 'lsblk' to know your devices" && exit

echo "[m]ount or [u]nmount?"
read action

case $action in
    m) udisksctl mount -b "$1" ;;
    u) udisksctl unmount -b "$1" && udisksctl power-off -b "$1" ;;
    *)
       echo "Usage: $0 /dev/sdXX"
       echo "Put apropriate disk name (check using lsblk)"
esac
