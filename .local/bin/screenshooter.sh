#!/bin/bash
# Dependencies:
# ImageMagick, xdotool, xclip

# Saved screenshoot
timestamp=$(date +%Y%m%d_%H%M%S)
filesc="$HOME/pix/screenshoot/magick-$timestamp.png"
tempsc="/tmp/screenshoot.png"

# Screenshoot image without shadow and copy it to clipboard
without_shadow() {
    magick import - | xclip -selection clipboard -t image/png
}

# With shadow
add_shadow() {
    magick import $1 - | magick convert - \( +clone -background black -shadow 50x20+0+20 \) +swap \
    -background none -layers merge +repage $filesc
    magick convert $filesc -bordercolor none -border 30 $filesc
}

# Copy screenshot to clipboard
copy_to_clipboard() {
    magick import $1 - | magick convert - \( +clone -background black -shadow 50x20+0+20 \) +swap \
    -background none -layers merge +repage $tempsc
    xclip -selection clipboard -t image/png -i $tempsc
    rm $tempsc
}

case $1 in
    -f)
        add_shadow "-window root"
        ;;
    -w)
        add_shadow "-window $(xdotool getactivewindow)"
        ;;
    -s)
        add_shadow
        ;;
    -sns)
        without_shadow
        ;;
    -fc)
        copy_to_clipboard "-window root"
        ;;
    -wc)
        copy_to_clipboard "-window $(xdotool getactivewindow)"
        ;;
    -sc)
        copy_to_clipboard
        ;;
    *)
        echo "Argument not found!"
        exit 1
        ;;
esac
