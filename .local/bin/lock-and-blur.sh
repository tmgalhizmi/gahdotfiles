#!/usr/bin/env bash

# set the icon and a temporary location for the screenshot to be stored
banner="$HOME/.local/bin/images/banner.png"
filesc='/tmp/screenshoot.png'

# take a screenshot
maim "$filesc"

# 1. scale image
#magick convert "$filesc" -scale 10% -scale 1000% "$filesc"

# 2. blur the screenshot by resizing and scaling back up
#convert "$filesc" -filter Gaussian -thumbnail 20% -sample 500% "$filesc"
# overlay the icon onto the screenshot
#convert "$filesc" "$icon" -gravity center -composite "$filesc"

# 3. blur screenshot
convert "$filesc" -blur 0x20 "$filesc"
convert "$filesc" "$banner" -gravity center -composite -matte "$filesc"

# lock the screen with the blurred screenshot
i3lock -i "$filesc"

# remove when unlocked
rm "$filesc"
