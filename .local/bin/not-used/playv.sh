#!/bin/sh
# Play all .mkv and .mp4 videos from current directory using mpv
# and take all subs with .srt format in the same directory

# subs=$( ls *.srt | tr '\n' ',' )
subs=$( ls *.srt | paste -sd ':' )
mpv *.mkv *.mp4 --sub-files="$subs"
