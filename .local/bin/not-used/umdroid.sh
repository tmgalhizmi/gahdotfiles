#!/bin/sh
# Script untuk memasang dan melepaskan perangkat MTP
# (Android, iOs dll)

# Menentukan dimana perangkat akan dipasang
mountpoint="$HOME/.local/share/android"

echo "[m]ount or [u]nmount?"
read action

[ ! -d $mountpoint ] && mkdir -pv $mountpoint

case $action in
    m) 
    simple-mtpfs -l
    echo -e "\nChoose device number."
    read dev

    simple-mtpfs --device $dev  $mountpoint ;;
    u) fusermount -u $mountpoint ;;
    *) echo "Unknown input!"
esac
