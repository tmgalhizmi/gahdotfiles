#!/bin/sh
# Script that let you set a wallpaper and place it in ~/.config/wal.png
# By tmgalhizmi

[ -f $1 ] && cp $1 $HOME/.config/wall.png && notify-send -i "$HOME/.config/wall.png" "Wallpaper has changed"
xwallpaper  --zoom $HOME/.config/wall.png
