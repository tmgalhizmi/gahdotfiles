#!/bin/sh
# high vol= 🔊  
# medium vol= 🔉
# low vol= 🔈
# mute= 🔇
#vol=$( amixer get Master | awk -F '[][]' 'END { print $2 }' )
vol=$( amixer get Master | tail -n1 | sed -r 's/.*\[(.*)%\].*/\1/' )
mute=$( amixer get Master | awk -F '[][]' 'END { print $4 }' )
# Problem in the mute variable in arch linux

if [ $vol -ge 75 ] && [ $mute == "on" ]; then
    volIcon="🔊"
elif [ $vol -le 75 ] && [ $vol -ge 45 ] && [ $mute == "on" ]; then
    volIcon="🔉"
#elif [ $mute == "off" ]; then
#    volIcon="🔇"
else
    volIcon="🔈"
fi

echo $volIcon $vol%
