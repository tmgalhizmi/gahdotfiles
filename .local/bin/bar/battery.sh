#!/usr/bin/env sh
# Battery status for awesome bar

capacity=$(cat /sys/class/power_supply/BAT1/capacity) || exit
status=$(cat /sys/class/power_supply/BAT1/status | sed -e "s/,//;s/Discharging/🔋/;s/Not charging/🛑/;s/Charging/🔌/;s/Unknown/♻️/;s/Full/⚡/;s/ 0*/ /g;s/ :/ /g")

[ $capacity -le 25 ] && [ $status = "🔋" ] && warn="❗"
printf "%s%s%s" "$status" "$warn" "$capacity%"
unset warn
