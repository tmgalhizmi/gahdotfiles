#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Source bash profile
#[[ -f ~/.bash_profile ]] && source ~/.bash_profile

# Activate vi mode with <Escape>:
#set -o vi

shopt -s expand_aliases
shopt -s histappend # enable history appending instead overwriting.
shopt -s autocd # auto "cd" directories

[[ $DISPLAY ]] && shopt -s checkwinsize

red='\[\e[0;31m\]'
RED='\[\e[1;31m\]'
green='\[\e[0;32m\]'
GREEN='\[\e[1;32m\]'
yellow='\[\e[0;33m\]'
YELLOW='\[\e[1;33m\]'
blue='\[\e[0;34m\]'
BLUE='\[\e[1;34m\]'
purple='\[\e[0;35m\]'
PURPLE='\[\e[1;35m\]'
cyan='\[\e[0;36m\]'
CYAN='\[\e[1;36m\]'
nc='\[\e[0m\]'

#PS1='[\u@\h \W]\$ '
# PS1="$RED[$nc$CYAN\u$nc$PURPLE@$nc$GREEN\h$nc $YELLOW\w$nc$RED]$nc\\n$BLUE\$$nc "

# Customize prompt
PS1="$RED[$nc$CYAN\u$nc$PURPLE@$nc$GREEN\h$nc$BLUE\$(__git_ps1 ' (%s)')$nc $YELLOW\w$nc$RED]$nc\\n$BLUE\$$nc "
#PS1="$GREEN\$(__git_ps1 ' (%s)')$nc $YELLOW\w$nc  $PURPLE>$nc "

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

[ -f ~/.config/aliasrc ] && . ~/.config/aliasrc # Running aliasrc

#[ -f /usr/share/doc/git-*/contrib/completion/git-prompt.sh ] && . /usr/share/doc/git-*/contrib/completion/git-prompt.sh # Git prompt for Slackware
[ -f /usr/share/git/git-prompt.sh ] && . /usr/share/git/git-prompt.sh # Git prompt on Arch Linux and Void Linux

export HISTCONTROL=ignoredups # bash only store one copy of each command.
HISTSIZE= HISTFILESIZE= # infinite history.

### cd into directory using fzf
# fda - including hidden directories
fda() {
  local dir
  dir=$(find ${1:-.} -type d 2> /dev/null | fzf +m) && cd "$dir"
}

# fe [FUZZY PATTERN] - Open the selected file with the default editor
#   - Bypass fuzzy finder if there's only one match (--select-1)
#   - Exit if there's no match (--exit-0)
fe() {
  # Original command from fzf wiki
  #IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
  #[[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
  # Problems: Multiple files can't open at the same time
  IFS=$'\n' files=$(find ${1:-.} 2> /dev/null | fzf-tmux --multi --select-1 --exit-0)
  if [[ -n "$files" ]]; then
      case $files in
          *.odt|*.doc?|*.ods|*.xls?|*.odp|*.ppt? ) libreoffice $files & ;;
          *.epub ) foliate $files & ;;
          *.pdf ) zathura $files & ;;
          *.jp?g|*.png ) sxiv $files & ;;
          *.wav|*.mp3|*.flac|*.m4a|*.wma|*.og[agx]|*.opus ) mpv $file ;;
          *.avi|*.mp4|*.wmv|*.3gp|*.ogv|*.mkv|*.mpg|*.mpeg|*.fl[icv]|*.webm|*.m4v ) mpv $files & ;;
          * ) ${EDITOR:-vim} "${files[@]}" ;;
      esac
  fi
}

# nnn cd on quit
ncoq ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, either remove the "export" as in:
    #    NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    #    (or, to a custom path: NNN_TMPFILE=/tmp/.lastd)
    # or, export NNN_TMPFILE after nnn invocation
    NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

# Start nnn in tmux
n () {
	if [ -n "$TMUX" ]; then
		nnn -a $@
	else
		tmux new-session nnn -a $@
	fi
}

# ranger cd on quit
# Compatible with ranger 1.4.2 through 1.9.*
#
# Automatically change the current working directory after closing ranger
#
# This is a shell function to automatically change the current working
# directory to the last visited one after ranger quits. Either put it into your
# .zshrc/.bashrc/etc or source this file from your shell configuration.
# To undo the effect of this function, you can type "cd -" to return to the
# original directory.

ranger_cd() {
    temp_file="$(mktemp -t "ranger_cd.XXXXXXXXXX")"
    ranger --choosedir="$temp_file" -- "${@:-$PWD}"
    if chosen_dir="$(cat -- "$temp_file")" && [ -n "$chosen_dir" ] && [ "$chosen_dir" != "$PWD" ]; then
        cd -- "$chosen_dir"
    fi
    rm -f -- "$temp_file"
}

# This binds Ctrl-O to ranger-cd:
bind '"\C-o":"ranger_cd\C-m"'

### Archive Extraction (From DT dotfiles)
# usage: ex <file>
ex ()
{
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)  tar xjf $1    ;;
            *.tar.gz)   tar xzf $1    ;;
            *.bz2)      bunzip2 $1    ;;
            *.rar)      unrar x $1    ;;
            *.gz)       gunzip $1     ;;
            *.tar)      tar xf $1     ;;
            *.tbz2)     tar xjf $1    ;;
            *.tgz)      tar xzf $1    ;;
            *.zip)      unzip $1      ;;
            *.Z)        uncompress $1 ;;
            *.7z)       7z x $1       ;;
            *.deb)      ar x $1       ;;
            *.tar.xz)   tar xf $1     ;;
            *.tar.zst)  unzstd $1     ;;
            *)          echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}
